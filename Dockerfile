FROM python:3.12

LABEL authors="michele"

RUN mkdir /app

WORKDIR /app

COPY . .

RUN apt-get update && apt-get install -y python3 && apt-get install -y python3-pip && pip3 install Flask

EXPOSE 8000

CMD ["python", "./app.py"]
